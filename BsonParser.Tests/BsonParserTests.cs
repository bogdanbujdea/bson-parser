﻿namespace Bson.Tests
{
    using NUnit.Framework;
    using FluentAssertions;

    public class BsonParserTests
    {
        private BinaryJsonParser _binaryJsonParser;
        private BinaryJsonBuilder _bsonBuilder;

        [SetUp]
        public void TestSetup()
        {
            _binaryJsonParser = new BinaryJsonParser(new BsonDataTypesParser(), new JsonBuilder());
            _bsonBuilder = new BinaryJsonBuilder();
        }

        [Test]
        public void create_json_with_empty_object()
        {
            var bson = _bsonBuilder
                .InsertBinaryObject(0)
                .GetBinaryJson();

            var json = _binaryJsonParser.BuildJson(bson);
            json.Should().Be("{}");
        }

        [Test]
        public void create_json_object_with_key_and_number()
        {
            _bsonBuilder.InsertBinaryObject(1)
                .InsertBinaryString("myNumber")
                .InsertBinaryNumber(100);
            var bson = _bsonBuilder.GetBinaryJson();
            var json = _binaryJsonParser.BuildJson(bson);
            json.Should().Be("{'myNumber':100}");
        }
        
        [Test]
        public void create_empty_json_list()
        {
            _bsonBuilder.InsertBinaryList(0);
            var bson = _bsonBuilder.GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[]");
        }

        [Test]
        public void create_list_with_key_value()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(1)
                .InsertBinaryObject(1)
                .InsertBinaryString("myNumber")
                .InsertBinaryNumber(20)
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[{'myNumber':20}]");
        }

        [Test]
        public void create_json_with_one_number()
        {
            var bson = _bsonBuilder
                .InsertBinaryNumber(1)
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("1");
        }

        [Test]
        public void create_list_with_one_number()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(1)
                .InsertBinaryNumber(1)
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[1]");
        }

        [Test]
        public void create_object_with_number_as_property()
        {
            var bson = _bsonBuilder
                .InsertBinaryObject(2)
                .InsertBinaryString("a")
                .InsertBinaryString("b")
                .InsertBinaryNumber(4)
                .InsertBinaryString("123")
                .GetBinaryJson();

            _binaryJsonParser.BuildJson(bson)
                .Should().Be("{'a':'b',4:'123'}");
        }

        [Test]
        public void create_json_with_deep_objects()
        {
            var bson = _bsonBuilder
                .InsertBinaryObject(2)
                .InsertBinaryString("a")
                .InsertBinaryString("b")
                .InsertBinaryString("object")
                .InsertBinaryObject(1)
                .InsertBinaryString("deep")
                .InsertBinaryObject(1)
                .InsertBinaryNumber(1)
                .InsertBinaryString("abc")
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("{'a':'b','object':{'deep':{1:'abc'}}}");
        }

        [Test]
        public void create_key_with_list_as_value()
        {
            var bson = _bsonBuilder
                .InsertBinaryObject(1)
                .InsertBinaryString("list")
                .InsertBinaryList(1)
                .InsertBinaryObject(1)
                .InsertBinaryString("a")
                .InsertBinaryString("b")
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("{'list':[{'a':'b'}]}");
        }

        [Test]
        public void create_list_with_empty_object()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(1)
                .InsertBinaryObject(0)
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[{}]");
        }

        [Test]
        public void create_string_array()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(2)
                .InsertBinaryString("a")
                .InsertBinaryString("b")
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("['a','b']");
        }

        [Test]
        public void create_numbers_array()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(2)
                .InsertBinaryNumber(1)
                .InsertBinaryNumber(2)
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[1,2]");
        }

        [Test]
        public void create_mixed_array()
        {
            var bson = _bsonBuilder
                .InsertBinaryList(3)
                .InsertBinaryNumber(1)
                .InsertBinaryNumber(2)
                .InsertBinaryObject(1)
                .InsertBinaryNumber(100)
                .InsertBinaryString("ab")
                .GetBinaryJson();
            _binaryJsonParser.BuildJson(bson)
                .Should().Be("[1,2,{100:'ab'}]");
        }
    }
}
