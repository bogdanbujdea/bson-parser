﻿namespace Bson.Tests
{
    using System;
    using System.Linq;
    using System.Text;
    
    using FluentAssertions;
    using NUnit.Framework;

    public class DataTypeParserTests
    {
        private BsonDataTypesParser _parser;

        [SetUp]
        public void TestsSetup()
        {
            _parser = new BsonDataTypesParser();            
        }

        [Test]
        public void parser_should_read_numbers()
        {
            const int number = 100;
            var bigEndianNumber = TestUtils.GetIntegerInBigEndianFormat(number);

            var result = _parser.ReadNumber(bigEndianNumber, 0);
            result.Should().Be(number);
        }

        [Test]
        public void parser_should_not_parse_invalid_byte_arrays()
        {
            var bytes = Encoding.UTF8.GetBytes("g");

            Action getInteger = () => _parser.ReadNumber(bytes, 0);
            
            getInteger.ShouldThrow<ArgumentException>()
                .WithMessage("Invalid number");
        }

        [Test]
        public void parser_should_read_strings()
        {
            const string text = "hello world!!!";
            var bytes = Encoding.UTF8.GetBytes(text).ToList();
            bytes.Add(0x00);
            var result = _parser.ReadString(bytes.ToArray(), 0);
            result.Should().Be(text);
        }

      
    }
}
