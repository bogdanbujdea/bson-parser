﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bson.Tests
{
    public class BinaryJsonBuilder
    {
        private List<byte> _binaryJson;

        public BinaryJsonBuilder()
        {
            _binaryJson = new List<byte>();
        }
        
        public BinaryJsonBuilder InsertBinaryNumber(int number)
        {
            InsertBigEndianNumber(number);
            return this;
        }

        private void InsertBigEndianNumber(int number)
        {
            _binaryJson.Add((byte)JsonTypes.Number);
            _binaryJson = _binaryJson.Concat(TestUtils.GetIntegerInBigEndianFormat(number)).ToList();
        }

        public BinaryJsonBuilder InsertBinaryObject(int keysCount)
        {
            _binaryJson.Add((byte)JsonTypes.Object);
            InsertBigEndianNumber(keysCount);
            return this;
        }

        public BinaryJsonBuilder InsertBinaryString(string key)
        {
            _binaryJson.Add((byte)JsonTypes.String);
            _binaryJson = _binaryJson.Concat(Encoding.UTF8.GetBytes(key)).ToList();
            _binaryJson.Add((byte)JsonTypes.Null);
            return this;
        }

        public BinaryJsonBuilder InsertBinaryList(int keyCount)
        {
            _binaryJson.Add((byte)JsonTypes.List);
            InsertBigEndianNumber(keyCount);
            return this;
        }

        public byte[] GetBinaryJson()
        {
            return _binaryJson.ToArray();
        }
    }
}