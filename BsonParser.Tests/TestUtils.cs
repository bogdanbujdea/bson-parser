﻿using System;
using System.Linq;

namespace Bson.Tests
{
    public static class TestUtils
    {
        public static byte[] GetIntegerInBigEndianFormat(int number)
        {
            var bytes = BitConverter.GetBytes(number);
            bytes = bytes.Reverse().ToArray();
            return bytes;
        }
    }
}
