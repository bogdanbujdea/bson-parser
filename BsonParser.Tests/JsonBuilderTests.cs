﻿using FluentAssertions;
using NUnit.Framework;

namespace Bson.Tests
{
    public class JsonBuilderTests
    {
        private JsonBuilder _jsonBuilder;

        [SetUp]
        public void TestSetup()
        {
            _jsonBuilder = new JsonBuilder();
        }

        [Test]
        public void create_empty_object()
        {
            _jsonBuilder.CreateObject().CloseObject();
            _jsonBuilder.BuildJson().Should().Be("{}");
        }

        [Test]
        public void create_empty_list()
        {
            _jsonBuilder.CreateList().CloseList();
            _jsonBuilder.BuildJson().Should().Be("[]");
        }

        [Test]
        public void create_object_with_one_key_value()
        {
            _jsonBuilder.CreateObject().InsertKey("myKey").InsertValue("myValue").CloseObject();
            _jsonBuilder.BuildJson().Should().Be("{'myKey':'myValue'}");
        }

        [Test]
        public void create_object_with_multiple_key_value()
        {
            _jsonBuilder.CreateObject()
                .InsertKey("myKey1")
                .InsertValue("myValue1")
                .InsertKey("myKey2")
                .InsertValue("myValue2")
                .InsertKey("myKey3")
                .InsertValue("myValue3")
                .CloseObject();
            _jsonBuilder.BuildJson().Should().Be("{'myKey1':'myValue1','myKey2':'myValue2','myKey3':'myValue3'}");
        }

        [Test]
        public void create_list_with_key_values()
        {
            _jsonBuilder.CreateList()
                .InsertKey("myKey1")
                .InsertValue("myValue1")
                .InsertKey("myKey2")
                .InsertValue("myValue2")
                .InsertKey("myKey3")
                .InsertValue("myValue3")
                .CloseList();
            _jsonBuilder.BuildJson().Should().Be("['myKey1':'myValue1','myKey2':'myValue2','myKey3':'myValue3']");
        }

        [Test]
        public void numbers_are_stored_as_integers()
        {
            _jsonBuilder.CreateObject()
                .InsertKey("myNumber")
                .InsertValue(100)
                .CloseObject()
                .BuildJson().Should().Be("{'myNumber':100}");
        }
    }
}
