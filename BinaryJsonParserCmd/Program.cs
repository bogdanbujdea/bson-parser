﻿namespace BinaryJsonParserCmd
{
    using System;
    using System.IO;

    using Bson;
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Initialize();
                if (HasValidParameters(args) == false)
                    ReadInfoFromUserInput();
                ParseAndWriteOutput();
                Console.WriteLine("SUCCESS!");
            }
            catch (Exception exception)
            {
                Console.WriteLine("An error occurred!");
                Console.WriteLine(exception.Message);
            }
            finally
            {
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
            }
        }

        private static string _resultFile;
        private static string _bsonPath;
        private static BinaryJsonParser _parser;

        private static void ParseAndWriteOutput()
        {
            _parser.BuildJson(File.ReadAllBytes(_bsonPath));
            var json = _parser.GetFormattedJson();
            if (_resultFile != null)
                File.WriteAllText(_resultFile, json);
        }

        private static void Initialize()
        {
            _resultFile = "result-" + new Random().Next(0, 1000) + ".txt";
            _bsonPath = string.Empty;
            _parser = new BinaryJsonParser(new BsonDataTypesParser(), new JsonBuilder());
        }

        private static void ReadInfoFromUserInput()
        {
            Console.WriteLine("Name of the input file: ");
            _bsonPath = Console.ReadLine();
            Console.WriteLine("Name of result file: ");
            var tmpFile = Console.ReadLine();
            if (string.IsNullOrEmpty(tmpFile) == false)
            {
                Console.WriteLine("WARNING: The json will be written in the file: " + _resultFile);
                _resultFile = tmpFile;
            }
        }

        private static bool HasValidParameters(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("You must type the path of the binary json");
                return false;
            }
            if (File.Exists(args[0]) == false)
            {
                Console.WriteLine("The binary json file doesn't exist. Please check it's name and path.");
                return false;
            }
            _bsonPath = args[0];
            if (args.Length < 2)
            {
                Console.WriteLine("WARNING: The json will be written in the file: " + _resultFile);
            }
            else
            {
                _resultFile = args[1];
            }
            return true;
        }
    }
}
