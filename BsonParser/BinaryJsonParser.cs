﻿namespace Bson
{
    using System.Collections.Generic;

    public enum ElementType
    {
        Object,
        List
    }

    public class BinaryJsonParser
    {        

        public BinaryJsonParser(IDataTypesParser dataTypesParser, IJsonBuilder jsonBuilder)
        {
            _dataTypesParser = dataTypesParser;
            _jsonBuilder = jsonBuilder;
        }

        public string BuildJson(byte[] bytes)
        {
            InitializeFields();
            ReadJson(bytes);
            return _jsonBuilder.BuildJson();
        }

        public string GetFormattedJson()
        {
            return _jsonBuilder.FormatJson();
        }

        private int _keysCount;
        private bool _keyInserted;
        private readonly IJsonBuilder _jsonBuilder;
        private List<ElementType> _openedContainers;
        private readonly IDataTypesParser _dataTypesParser;

        private void InitializeFields()
        {
            _keyInserted = false;
            _openedContainers = new List<ElementType>();
            _keysCount = 0;
        }
        private void ReadJson(byte[] bytes)
        {
            for (var i = 0; i < bytes.Length; i++)
            {
                switch ((JsonTypes)bytes[i])
                {
                    case JsonTypes.Object:
                        CreateObject(bytes, _openedContainers, ref i);
                        _keyInserted = false;
                        break;
                    case JsonTypes.Number:
                        ReadNumber(bytes, i, _openedContainers);
                        i += 4;
                        break;
                    case JsonTypes.String:
                        i += ReadString(bytes, i, _openedContainers);
                        break;
                    case JsonTypes.List:
                        ReadList(bytes, _openedContainers, ref i);
                        _keyInserted = false;
                        i += 4;
                        break;
                }
            }
            CloseRemainingContainers();

        }

        private void ReadList(byte[] bytes, List<ElementType> openedElements, ref int i)
        {
            _jsonBuilder.CreateList();
            openedElements.Insert(0, ElementType.List);
            i++;
            _keysCount = _dataTypesParser.ReadNumber(bytes, i + 1);
        }

        private int ReadString(byte[] bytes, int i, List<ElementType> openedElements)
        {
            var data = _dataTypesParser.ReadString(bytes, i + 1);
            if (!_keyInserted || _openedContainers[0] == ElementType.List)
            {
                _jsonBuilder.InsertKey(data);
                _keyInserted = true;
            }
            else
            {
                _jsonBuilder.InsertValue(data);
                _keyInserted = false;
                _keysCount--;
                
                if (_keysCount != 0) return data.Length + 1;

                var lastOpenedElement = openedElements[0];
                CloseContainer(lastOpenedElement);
                openedElements.RemoveAt(0);
            }
            return data.Length + 1;
        }

        private void ReadNumber(byte[] bytes, int index, List<ElementType> openedElements)
        {
            var number = _dataTypesParser.ReadNumber(bytes, index + 1);
            if (!_keyInserted || _openedContainers[0] == ElementType.List)
            {
                _jsonBuilder.InsertKey(number);
                _keyInserted = true;
            }
            else
            {
                _jsonBuilder.InsertValue(number);
                _keysCount--;
                if (_keysCount == 0)
                {
                    var lastOpenedElement = openedElements[0];
                    CloseContainer(lastOpenedElement);
                    openedElements.RemoveAt(0);
                }
                _keyInserted = false;
            }
        }

        private void CreateObject(byte[] bytes, List<ElementType> openedElements, ref int i)
        {
            _jsonBuilder.CreateObject();
            openedElements.Insert(0, ElementType.Object);
            i++;
            _keysCount = _dataTypesParser.ReadNumber(bytes, i + 1);
            i += 4;
        }

        private void CloseContainer(ElementType openedContainer)
        {
            if (openedContainer == ElementType.List)
                _jsonBuilder.CloseList();
            else
                _jsonBuilder.CloseObject();
        }

        private void CloseRemainingContainers()
        {
            foreach (var openedContainer in _openedContainers)
            {
                CloseContainer(openedContainer);
            }

        }
    }
}