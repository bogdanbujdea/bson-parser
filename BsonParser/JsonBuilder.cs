﻿namespace Bson
{
    using System.Linq;
    using System.Text;

    public enum JsonTypes : byte
    {
        Number = 0x01,
        String = 0x02,
        List = 0x05,
        Object = 0x06,
        Null = 0x00
    }

    public class JsonBuilder : IJsonBuilder
    {
        private readonly StringBuilder _json;
        private int _tabCount;
        private StringBuilder _formattedJson;

        public JsonBuilder()
        {
            _json = new StringBuilder();
        }

        public JsonBuilder CreateObject()
        {
            if (_json.Length == 0)
            {
                _json.Append('{');
                return this;
            }

            var lastItem = _json.ToString().Last();

            if (ItemIsInsideArray() && lastItem != '[')
            {
                _json.Append(",{");
            }
            else if (lastItem == '[')
                _json.Append('{');
            else
                _json.Append(":{");
            return this;
        }

        public JsonBuilder CloseObject()
        {
            _json.Append('}');
            return this;
        }

        public JsonBuilder CreateList()
        {
            if (_json.Length == 0)
            {
                _json.Append('[');
                return this;
            }
            var lastItem = _json.ToString().Last();
            if (ItemIsInsideArray() && lastItem == ']' || lastItem == '}')
                _json.Append(",[");
            else
            {
                _json.Append(":[");
            }
            return this;
        }

        public JsonBuilder CloseList()
        {
            _json.Append(']');
            return this;
        }

        public JsonBuilder InsertKey(object key)
        {
            if (_json.Length != 0)
            {
                var lastItem = _json.ToString().Last();

                if (lastItem != '{' && lastItem != '[')
                {
                    _json.Append(',');
                }
            }
            if (key is int)
                _json.Append(key);
            else
                _json.Append("'" + key + "'");
            return this;
        }

        public JsonBuilder InsertValue(object value)
        {
            if (value is int)
                _json.Append(":" + value);
            else
                _json.Append(":'" + value + "'");
            return this;
        }

        public string BuildJson()
        {
            return _json.ToString();
        }

        public string FormatJson()
        {
            var json = _json.ToString();
            _formattedJson = new StringBuilder();
            _tabCount = 0;
            for (int index = 0; index < json.Length; index++)
            {
                var ch = json[index];
                switch (ch)
                {
                    case '{':
                        _tabCount++;
                        _formattedJson.Append(ch);
                        AppendLineAndTabs();
                        break;
                    case '[':
                        _formattedJson.Append(ch);
                        _tabCount++;
                        AppendLineAndTabs();
                        break;
                    case ',':
                        _formattedJson.Append(ch);
                        AppendLineAndTabs();
                        break;
                    case ':':
                        _formattedJson.Append(ch);
                        _formattedJson.Append(' ');
                        break;
                    case '}':
                        _tabCount--;
                        AppendLineAndTabs();
                        _formattedJson.Append(ch);
                        if (index + 1 < json.Length && json[index + 1] == ',')
                        {
                            _formattedJson.Append(',');
                            AppendLineAndTabs();
                            index++;
                        }
                        break;
                    case ']':
                        _tabCount--;
                        AppendLineAndTabs();
                        _formattedJson.Append(ch);
                        break;
                    default:
                        _formattedJson.Append(ch);
                        break;
                }
            }
            return _formattedJson.ToString();
        }

        private void AppendLineAndTabs()
        {
            _formattedJson.AppendLine();
            AddTabs();
        }

        private void AddTabs()
        {
            for (int i = 0; i < _tabCount; i++)
            {
                _formattedJson.Append('\t');
            }
        }

        private bool ItemIsInsideArray()
        {
            var countOfOpenedBraces = _json.ToString().Count(c => c == '[');
            var countOfClosedBraces = _json.ToString().Count(c => c == ']');
            return countOfClosedBraces < countOfOpenedBraces;
        }


    }
}
