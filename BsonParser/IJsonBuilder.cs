﻿namespace Bson
{
    public interface IJsonBuilder
    {
        JsonBuilder CreateObject();
        JsonBuilder CloseObject();
        JsonBuilder CreateList();
        JsonBuilder CloseList();
        JsonBuilder InsertKey(object key);
        JsonBuilder InsertValue(object value);
        string BuildJson();
        string FormatJson();
    }
}