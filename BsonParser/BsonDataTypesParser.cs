﻿namespace Bson
{
    using System;
    using System.Text;
    using System.Collections.Generic;

    public class BsonDataTypesParser : IDataTypesParser
    {

        public int ReadNumber(byte[] bytes, int index)
        {
            if (bytes.Length < 4)
                throw new ArgumentException("Invalid number");

            var number = GetBytesForNumber(bytes, index);
            Array.Reverse(number);
            return BitConverter.ToInt32(number, 0);
        }

        private byte[] GetBytesForNumber(byte[] bytes, int index)
        {
            var number = new byte[4];
            for (var i = 0; i < 4; i++)
            {
                number[i] = bytes[index++];
            }
            return number;
        }

        public string ReadString(byte[] bytes, int index)
        {
            var binaryString = new List<byte>();
            while (index < bytes.Length && bytes[index] != 0x00)
            {
               binaryString.Add(bytes[index++]); 
            }
            return Encoding.UTF8.GetString(binaryString.ToArray());
        }
    }
}