﻿namespace Bson
{
    public interface IDataTypesParser
    {
        int ReadNumber(byte[] bytes, int index);
        string ReadString(byte[] bytes, int index);
    }
}