You can go in the folder of the console application
and change the Program.cs file. After that, you should
execute compile.bat, and run.bat to run the program.

The program can be run from the command line.

>Program.exe input_1 result_1

the first parameter is the binary json
the second parameter is the file for the parsed json(it will 
be created if it doesn't exist)

You can also start the program by double clicking it.
It will prompt you for the name of the input file and the
name of the result file.

If you want to copy the program in another folder and execute it there, you should
copy the "Bson.dll" file along with it, or else it won't work.