The project is divided in three separate projects.
1. BinaryJsonParserCmd
	-this is a console application which you can use to parse binary json files
	-if you want to compile and run it without visual studio, you should
	 go to the "app" folder and execute compile.bat and run.bat
2. BsonParser
	-this is the binary json parser
	-it's a library that has only the logic for parsing bson files, creating json files, etc.
3. BsonParser.Tests
	-here are the tests for the BsonParser library
	-you can run them if you open the "run_tests.bat" file in the "app" folder

You can also find the source code on BitBucker

https://bitbucket.org/bogdanbujdea/bson-parser